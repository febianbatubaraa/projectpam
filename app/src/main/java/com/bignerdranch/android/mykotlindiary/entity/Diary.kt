package com.bignerdranch.android.mykotlindiary.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Diary(
    var id: Int = 0,
    var title: String? = null,
    var description: String? = null,
    var date: String? = null,
    var photo: ByteArray? = null
) : Parcelable