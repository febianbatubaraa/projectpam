package com.bignerdranch.android.mykotlindiary.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bignerdranch.android.mykotlindiary.CustomOnItemClickListener
import com.bignerdranch.android.mykotlindiary.DiaryAddUpdateActivity
import com.bignerdranch.android.mykotlindiary.R
import com.bignerdranch.android.mykotlindiary.entity.Diary
import kotlinx.android.synthetic.main.item_diary.view.*

class DiaryAdapter(private val activity: Activity) :
    RecyclerView.Adapter<DiaryAdapter.NoteViewHolder>() {
    var listDiaries = ArrayList<Diary>()
        set(listDiaries) {
            if (listDiaries.size > 0) {
                this.listDiaries.clear()
            }
            this.listDiaries.addAll(listDiaries)

            notifyDataSetChanged()
        }

    fun addItem(diary: Diary) {
        this.listDiaries.add(diary)
        notifyItemInserted(this.listDiaries.size - 1)
    }

    fun updateItem(position: Int, diary: Diary) {
        this.listDiaries[position] = diary
        notifyItemChanged(position, diary)
    }

    fun removeItem(position: Int) {
        this.listDiaries.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, this.listDiaries.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_diary, parent, false)
        return NoteViewHolder(view)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(listDiaries[position])
    }

    override fun getItemCount(): Int = this.listDiaries.size

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(diary: Diary) {
            with(itemView) {
                tv_item_title.text = diary.title
                tv_item_date.text = diary.date
                tv_item_description.text = diary.description
                if (diary.photo != null) {
                    tv_item_image.visibility = View.VISIBLE
                }
                cv_item_diary.setOnClickListener(
                    CustomOnItemClickListener(
                        adapterPosition,
                        object : CustomOnItemClickListener.OnItemClickCallback {
                            override fun onItemClicked(view: View, position: Int) {
                                val intent = Intent(activity, DiaryAddUpdateActivity::class.java)
                                intent.putExtra(DiaryAddUpdateActivity.EXTRA_POSITION, position)
                                intent.putExtra(DiaryAddUpdateActivity.EXTRA_DIARY, diary)
                                activity.startActivityForResult(
                                    intent,
                                    DiaryAddUpdateActivity.REQUEST_UPDATE
                                )
                            }
                        })
                )
            }
        }
    }
}