package com.bignerdranch.android.mykotlindiary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.bignerdranch.android.mykotlindiary.adapter.DiaryAdapter
import com.bignerdranch.android.mykotlindiary.db.DiaryHelper
import com.bignerdranch.android.mykotlindiary.entity.Diary
import com.bignerdranch.android.mykotlindiary.helper.MappingHelper
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: DiaryAdapter
    private lateinit var diaryHelper: DiaryHelper

    companion object {
        private const val EXTRA_STATE = "EXTRA_STATE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = "My Diary"
        rv_diary.layoutManager = LinearLayoutManager(this)
        rv_diary.setHasFixedSize(true)
        adapter = DiaryAdapter(this)
        rv_diary.adapter = adapter

        fab_add.setOnClickListener {
            val intent = Intent(this@MainActivity, DiaryAddUpdateActivity::class.java)
            startActivityForResult(intent, DiaryAddUpdateActivity.REQUEST_ADD)
        }

        diaryHelper = DiaryHelper.getInstance(applicationContext)
        diaryHelper.open()

        if (savedInstanceState == null) {
            loadNotesAsync()
        } else {
            val list = savedInstanceState.getParcelableArrayList<Diary>(EXTRA_STATE)
            if (list != null) {
                adapter.listDiaries = list
            }
        }
    }

    private fun loadNotesAsync() {
        GlobalScope.launch(Dispatchers.Main) {
            progressbar.visibility = View.VISIBLE
            val deferredNotes = async(Dispatchers.IO) {
                val cursor = diaryHelper.queryAll()
                MappingHelper.mapCursorToArrayList(cursor)
            }
            progressbar.visibility = View.INVISIBLE
            val diaries = deferredNotes.await()
            if (diaries.size > 0) {
                adapter.listDiaries = diaries
            } else {
                adapter.listDiaries = ArrayList()
                showSnackbarMessage("Tidak ada data diary saat ini")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                DiaryAddUpdateActivity.REQUEST_ADD -> if (resultCode == DiaryAddUpdateActivity.RESULT_ADD) {
                    val note = data.getParcelableExtra<Diary>(DiaryAddUpdateActivity.EXTRA_DIARY)
                    adapter.addItem(note)
                    rv_diary.smoothScrollToPosition(adapter.itemCount - 1)
                    showSnackbarMessage("Satu item berhasil ditambahkan")
                }
                DiaryAddUpdateActivity.REQUEST_UPDATE ->
                    when (resultCode) {
                        DiaryAddUpdateActivity.RESULT_UPDATE -> {
                            val note =
                                data.getParcelableExtra<Diary>(DiaryAddUpdateActivity.EXTRA_DIARY)
                            val position =
                                data.getIntExtra(DiaryAddUpdateActivity.EXTRA_POSITION, 0)
                            adapter.updateItem(position, note)
                            rv_diary.smoothScrollToPosition(position)
                            showSnackbarMessage("Satu item berhasil diubah")
                        }
                        DiaryAddUpdateActivity.RESULT_DELETE -> {
                            val position =
                                data.getIntExtra(DiaryAddUpdateActivity.EXTRA_POSITION, 0)
                            adapter.removeItem(position)
                            showSnackbarMessage("Satu item berhasil dihapus")
                        }
                    }
            }
        }
    }

    private fun showSnackbarMessage(message: String) {
        Snackbar.make(rv_diary, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        diaryHelper.close()
    }
}
