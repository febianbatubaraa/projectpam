package com.bignerdranch.android.mykotlindiary

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bignerdranch.android.mykotlindiary.db.DatabaseContract
import com.bignerdranch.android.mykotlindiary.db.DiaryHelper
import com.bignerdranch.android.mykotlindiary.entity.Diary
import kotlinx.android.synthetic.main.activity_diary_add_update.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class DiaryAddUpdateActivity : AppCompatActivity(), View.OnClickListener {

    private var isEdit = false
    private var diary: Diary? = null
    private var position: Int = 0
    private lateinit var diaryHelper: DiaryHelper
    private lateinit var diaryPhotoView: ImageView
    lateinit var currentPhotoPath: String

    companion object {
        const val EXTRA_DIARY = "extra_diary"
        const val EXTRA_POSITION = "extra_position"
        const val REQUEST_ADD = 100
        const val RESULT_ADD = 101
        const val REQUEST_UPDATE = 200
        const val RESULT_UPDATE = 201
        const val RESULT_DELETE = 301
        const val REQUEST_TAKE_PHOTO = 1
        const val ALERT_DIALOG_CLOSE = 10
        const val ALERT_DIALOG_DELETE = 20
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diary_add_update)

        diaryHelper = DiaryHelper.getInstance(applicationContext)
        diaryHelper.open()
        diaryPhotoView = findViewById(R.id.diary_photo)
        diary = intent.getParcelableExtra(EXTRA_DIARY)
        if (diary != null) {
            position = intent.getIntExtra(EXTRA_POSITION, 0)
            isEdit = true
        } else {
            diary = Diary()
        }
        val actionBarTitle: String
        val btnTitle: String
        if (isEdit) {
            actionBarTitle = "Ubah"
            btnTitle = "Update"
            diary?.let {
                edt_title.setText(it.title)
                edt_description.setText(it.description)

                val diaryImage: ByteArray? = it.photo
                val bitmap =
                    diaryImage?.size?.let { it1 ->
                        BitmapFactory.decodeByteArray(
                            diaryImage, 0,
                            it1
                        )
                    }
                if (diaryImage != null) {
                    diaryPhotoView.setImageBitmap(bitmap)
                    diaryPhotoView.visibility = View.VISIBLE
                }
            }
        } else {
            actionBarTitle = "Tambah"
            btnTitle = "Simpan"
        }
        supportActionBar?.title = actionBarTitle
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        btn_submit.text = btnTitle

        btn_submit.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btn_submit) {
            val title = edt_title.text.toString().trim()
            val description = edt_description.text.toString().trim()
            val diaryPhotoByte: ByteArray

            if (title.isEmpty()) {
                edt_title.error = "Field can not be blank"
                return
            }

            diary?.title = title
            diary?.description = description

            val intent = Intent()
            intent.putExtra(EXTRA_DIARY, diary)
            intent.putExtra(EXTRA_POSITION, position)

            val values = ContentValues()
            values.put(DatabaseContract.DiaryColumns.TITLE, title)
            values.put(DatabaseContract.DiaryColumns.DESCRIPTION, description)

            if (diaryPhotoView.drawable != null) {
                diaryPhotoByte = imageViewToByte(diaryPhotoView)
                diary?.photo = diaryPhotoByte
                values.put(DatabaseContract.DiaryColumns.PHOTO, diaryPhotoByte)
            }

            if (isEdit) {
                val result = diaryHelper.update(diary?.id.toString(), values).toLong()
                if (result > 0) {
                    setResult(RESULT_UPDATE, intent)
                    finish()
                } else {
                    Toast.makeText(
                        this@DiaryAddUpdateActivity,
                        "Gagal mengupdate data",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                diary?.date = getCurrentDate()
                values.put(DatabaseContract.DiaryColumns.DATE, getCurrentDate())
                val result = diaryHelper.insert(values)
                if (result > 0) {
                    diary?.id = result.toInt()
                    setResult(RESULT_ADD, intent)
                    finish()
                } else {
                    Toast.makeText(
                        this@DiaryAddUpdateActivity,
                        "Gagal menambah data",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun imageViewToByte(image: ImageView): ByteArray {
        val bitmap = (image.drawable as BitmapDrawable).bitmap
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }

    private fun getCurrentDate(): String {
        val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault())
        val date = Date()
        return dateFormat.format(date)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (isEdit) {
            menuInflater.inflate(R.menu.menu_form, menu)
        } else {
            menuInflater.inflate(R.menu.menu_add, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_delete -> showAlertDialog(ALERT_DIALOG_DELETE)
            R.id.action_take_picture -> dispatchTakePictureIntent()
            android.R.id.home -> showAlertDialog(ALERT_DIALOG_CLOSE)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        showAlertDialog(ALERT_DIALOG_CLOSE)
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        when (requestCode) {
//            REQUEST_TAKE_PHOTO -> {
//                updatePhotoView()
//            }
//        }
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            diaryPhotoView.visibility = View.VISIBLE
            diaryPhotoView.setImageBitmap(imageBitmap)
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    private fun showAlertDialog(type: Int) {
        val isDialogClose = type == ALERT_DIALOG_CLOSE
        val dialogTitle: String
        val dialogMessage: String
        if (isDialogClose) {
            dialogTitle = "Batal"
            dialogMessage = "Apakah anda ingin membatalkan perubahan pada diary?"
        } else {
            dialogMessage = "Apakah anda yakin ingin menghapus item diary ini?"
            dialogTitle = "Hapus Diary"
        }
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(dialogTitle)
        alertDialogBuilder
            .setMessage(dialogMessage)
            .setCancelable(false)
            .setPositiveButton("Ya") { dialog, id ->
                if (isDialogClose) {
                    finish()
                } else {
                    val result = diaryHelper.deleteById(diary?.id.toString()).toLong()
                    if (result > 0) {
                        val intent = Intent()
                        intent.putExtra(EXTRA_POSITION, position)
                        setResult(RESULT_DELETE, intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@DiaryAddUpdateActivity,
                            "Gagal menghapus data",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            .setNegativeButton("Tidak") { dialog, id -> dialog.cancel() }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}