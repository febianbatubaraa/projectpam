package com.bignerdranch.android.mykotlindiary.helper

import android.database.Cursor
import com.bignerdranch.android.mykotlindiary.db.DatabaseContract
import com.bignerdranch.android.mykotlindiary.entity.Diary

object MappingHelper {

    fun mapCursorToArrayList(diariesCursor: Cursor?): ArrayList<Diary> {
        val diariesList = ArrayList<Diary>()
        diariesCursor?.apply {
            while (moveToNext()) {
                val id = getInt(getColumnIndexOrThrow(DatabaseContract.DiaryColumns._ID))
                val title = getString(getColumnIndexOrThrow(DatabaseContract.DiaryColumns.TITLE))
                val description =
                    getString(getColumnIndexOrThrow(DatabaseContract.DiaryColumns.DESCRIPTION))
                val date = getString(getColumnIndexOrThrow(DatabaseContract.DiaryColumns.DATE))
                val photo = getBlob(getColumnIndexOrThrow(DatabaseContract.DiaryColumns.PHOTO))
                diariesList.add(Diary(id, title, description, date, photo))
            }
        }
        return diariesList
    }
}