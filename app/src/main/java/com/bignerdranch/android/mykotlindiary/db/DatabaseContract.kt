package com.bignerdranch.android.mykotlindiary.db

import android.provider.BaseColumns

internal class DatabaseContract {
    internal class DiaryColumns : BaseColumns {
        companion object {
            const val TABLE_NAME = "diary"
            const val _ID = "_id"
            const val TITLE = "title"
            const val DESCRIPTION = "description"
            const val DATE = "date"
            const val PHOTO = "photo"
        }
    }
}